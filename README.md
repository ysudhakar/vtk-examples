This repository contains a few examples of VTK file formats.  
VTK is widely used in visualization of numerical simulation results
The variants of vtk files mentioned here can be directly opened in
Paraview (https://www.paraview.org/) and Visit (https://wci.llnl.gov/simulation/computer-codes/visit/)
All examples use XML file format that will make it possible to use recent features.

The folders seriel and parallel contain, respectively, examples of serial and parallel data.
